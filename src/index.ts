import * as angular from 'angular';
import '@uirouter/angularjs';

import { coreModule } from './core/core.module';

angular.module('app', [
    'ui.router',
    coreModule
]);

angular.element(document).ready(() => {
    angular.bootstrap(document, ['app'], { strictDi: true });
});