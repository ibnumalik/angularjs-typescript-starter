export class UserComponent implements ng.IComponentOptions {
    static NAME: string = 'userComponent';
    template: string;

    constructor() {
        this.template = `
            <h2>User Component</h2>
        `;
    }
}