import { UserComponent } from './user.component';
import * as angular from 'angular';

export const userModule =
    angular
        .module('user', [])
        .component(UserComponent.NAME, new UserComponent)
        .name;