import * as angular from "angular";

import { AppRootComponent } from './app-root/app-root.component';
import { userModule } from './../user/user.module';
import { routing } from './core.routes';

export const coreModule =
  angular
    .module('app.core', [
        userModule
    ])
    .component(AppRootComponent.NAME, new AppRootComponent)
    .config(routing)
    .name;