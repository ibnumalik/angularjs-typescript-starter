import { AppRootComponent } from './app-root/app-root.component';

export const routing =
($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
    'ngInject';

    $stateProvider
        .state('root', {
            url: '/',
            component: AppRootComponent.NAME
        });

    $urlRouterProvider.otherwise('/');
}