export class AppRootComponent implements ng.IComponentOptions {
    static NAME: string = 'appRoot';
    public template: string;

    constructor() {
        this.template = ` <div ui-view></div> `;
    }
}