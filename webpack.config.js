const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
    return {
        entry: {
            app: './src/index.ts'
        },

        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: [
                        'ng-annotate-loader',
                        'ts-loader'
                    ]
                }
            ]
        },

        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        chunks: 'all'
                    }
                }
            }
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html'
            })
        ],

        resolve: {
            extensions: ['.ts', '.js']
        }
    }
}